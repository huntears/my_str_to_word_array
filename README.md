# my_str_to_word_array

Epitech compliant my_str_to_word_array implementation.

This can be used as a reference implementation.

## Multiple implementations

There are two implementations:
 - The first does two allocations, one to dupe the original string and one to
    store the lookup table to the words
 - The second one does only one allocation and packs both the lookup table and
    the string dupe in the same allocations

Both allocate the same number of bytes in the end, it's just that the
optimized version does the operation faster and is easier to free as you only
have to call free once with the start pointer of the lookup table.

## Banned functions

Of course this implementation uses banned function at Epitech so watch out :)
