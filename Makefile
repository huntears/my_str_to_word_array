##
## EPITECH PROJECT, 2023
## my_str_to_word_array
## File description:
## Really simple Makefile cause augh
##

CFLAGS	:=	-Wall -Wextra -Werror -O3 -flto -Weverything -Wno-unsafe-buffer-usage -D_FORTIFY_SOURCE=2

LDFLAGS	:=	-flto -s

CC		:=	clang

all: simple optimized

simple: main.o my_str_to_word_array.o
	${CC} ${LDFLAGS} main.o my_str_to_word_array.o -o simple

optimized: main.o my_str_to_word_array_optimized.o
	${CC} ${LDFLAGS} main.o my_str_to_word_array_optimized.o -o optimized

clean:
	rm -f *.o

fclean: clean
	rm -f simple optimized
