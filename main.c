/*
** EPITECH PROJECT, 2023
** my_str_to_word_array
** File description:
** main
*/

#include <stddef.h>
#include <stdio.h>

#include "my_str_to_word_array.h"

int main(void)
{
    char test_string[] = "I really like pasta you know, they are pretty good!";
    char **word_array = my_str_to_word_array(test_string);

    if (!word_array)
        return 84;
    for (int i = 0; word_array[i] != NULL; i++)
        printf("%s\n", word_array[i]);
    return 0;
}
